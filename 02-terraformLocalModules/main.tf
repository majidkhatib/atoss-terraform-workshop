terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.108.0"
    }
  }
}

provider "azurerm" {
  skip_provider_registration = true
  features {}
}

terraform {
  backend "local" {}
}

resource "azurerm_resource_group" "hello_world" {
  name     = var.resource_group_name
  location = var.location
}

module "networking" {
  source = "./modules/networking"

  name_prefix         = var.name_prefix
  resource_group_name = azurerm_resource_group.hello_world.name
  location            = var.location
  subscription_name   = var.subscription_name

  vnet_address_space_prefix = var.vnet_address_space_prefix

  depends_on = [ azurerm_resource_group.hello_world ]
}

module "vm" {
  source = "./modules/vm"

  name_prefix         = var.name_prefix
  resource_group_name = azurerm_resource_group.hello_world.name
  location            = var.location

  network_interface_id = module.networking.network_interface_id

  vm_public_ssh_keys       = var.vm_public_ssh_keys
  vm_auto_shutdown_enabled = var.vm_auto_shutdown_enabled
  vm_auto_shutdown_time    = var.vm_auto_shutdown_time
  vm_size                  = var.vm_size

  depends_on = [ module.networking ]
}
