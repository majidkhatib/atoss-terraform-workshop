variable "name_prefix" {
  type = string
}

variable "resource_group_name" {
  type = string
}

variable "location" {
  type = string
}

variable "subscription_slug" {
  type = string
}

variable "subscription_name" {
  type = string
}

variable "vnet_address_space_prefix" {
  type = string
}

variable "vm_public_ssh_keys" {
  type = list(string)
}

variable "vm_auto_shutdown_enabled" {
  type    = bool
  default = true
}

variable "vm_auto_shutdown_time" {
  type    = string
  default = "2000"
}

variable "vm_size" {
  type    = string
  default = "Standard_B2s"
}
