terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.108.0"
    }
  }
}

provider "azurerm" {
  skip_provider_registration = true
  features {}
}

terraform {
  backend "local" {}
}

resource "azurerm_resource_group" "hello_world" {
  name     = var.resource_group_name
  location = var.location
}
