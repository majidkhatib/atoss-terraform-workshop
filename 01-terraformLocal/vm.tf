resource "azurerm_linux_virtual_machine" "hello_world" {
  name                = "${var.name_prefix}-vm"
  resource_group_name = azurerm_resource_group.hello_world.name
  location            = var.location
  size                = var.vm_size
  admin_username      = "azureuser"
  network_interface_ids = [
    azurerm_network_interface.vm.id,
  ]

  dynamic "admin_ssh_key" {
    for_each = var.vm_public_ssh_keys
    content {
      username   = "azureuser"
      public_key = file(admin_ssh_key.value)
    }
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "debian"
    offer     = "debian-11"
    sku       = "11-gen2"
    version   = "latest"
  }
}

resource "azurerm_dev_test_global_vm_shutdown_schedule" "db_vm_connection" {
  virtual_machine_id = azurerm_linux_virtual_machine.hello_world.id
  location           = var.location
  enabled            = var.vm_auto_shutdown_enabled

  daily_recurrence_time = var.vm_auto_shutdown_time
  timezone              = "W. Europe Standard Time"

  notification_settings {
    enabled = false
  }
}
