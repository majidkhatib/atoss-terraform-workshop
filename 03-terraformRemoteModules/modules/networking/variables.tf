variable "name_prefix" {
  type = string
}

variable "resource_group_name" {
  type = string
}

variable "location" {
  type = string
}

variable "subscription_name" {
  type = string
}

variable "vnet_address_space_prefix" {
  type = string
}
