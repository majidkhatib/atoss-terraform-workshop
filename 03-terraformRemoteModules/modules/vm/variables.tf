variable "name_prefix" {
  type = string
}

variable "resource_group_name" {
  type = string
}

variable "location" {
  type = string
}

variable "network_interface_id" {
  type = string
}

variable "vm_public_ssh_keys" {
  type = list(string)
}

variable "vm_auto_shutdown_enabled" {
  type = bool
}

variable "vm_auto_shutdown_time" {
  type = string
}

variable "vm_size" {
  type = string
}
