name_prefix         = "miri"
resource_group_name = "HELLO-WORLD-MIRI"
location            = "West Europe"

subscription_slug = "14391fe5"
subscription_name = "miri"

vnet_address_space_prefix = "10.0"
vm_public_ssh_keys        = ["~/.ssh/2024-06_tf-workshop.pub"]
