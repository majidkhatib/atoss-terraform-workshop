locals {
  resource_group_prefix = "HELLO-WORLD"
  location              = "West Europe"

  subscription_slug = "14391fe5"
  subscription_name = "miri"
}

remote_state {
  backend = "azurerm"
  config = {
    resource_group_name  = "MIRI-TF-REMOTE-STATE"
    storage_account_name = "miritfremotestate"
    container_name       = "remote-states"
    # key: must be specified per step
  }
}

generate "provider" {
  path      = "required_providers.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.108.0"
    }
  }
}
EOF
}
