resource "azurerm_virtual_network" "hello_world" {
  name                = "${var.name_prefix}-${var.subscription_name}"
  resource_group_name = var.resource_group_name
  location            = var.location
  address_space       = [format("%s.0.0/16", var.vnet_address_space_prefix)]
}

resource "azurerm_subnet" "vm" {
  name                 = "vm"
  resource_group_name  = var.resource_group_name
  virtual_network_name = azurerm_virtual_network.hello_world.name
  address_prefixes     = [format("%s.4.0/24", var.vnet_address_space_prefix)]

  service_endpoints = ["Microsoft.Storage"]
}

resource "azurerm_network_security_group" "vm" {
  name                = "${var.name_prefix}-nsg"
  location            = var.location
  resource_group_name = var.resource_group_name

  security_rule {
    name                         = "SSH"
    priority                     = 300
    direction                    = "Inbound"
    access                       = "Allow"
    protocol                     = "Tcp"
    source_port_range            = "*"
    destination_port_range       = "22"
    source_address_prefix        = "*"
    destination_address_prefix   = "*"
    source_address_prefixes      = []
    destination_address_prefixes = []
  }
}

resource "azurerm_public_ip" "vm" {
  name                = "${var.name_prefix}-vm"
  resource_group_name = var.resource_group_name
  location            = var.location
  allocation_method   = "Static"
  sku                 = "Standard"
}

resource "azurerm_network_interface" "vm" {
  name                = "${var.name_prefix}-vm"
  resource_group_name = var.resource_group_name
  location            = var.location

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.vm.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.vm.id
  }
}

resource "azurerm_network_interface_security_group_association" "vm" {
  network_interface_id      = azurerm_network_interface.vm.id
  network_security_group_id = azurerm_network_security_group.vm.id
}
