include "base" {
  path   = "${get_terragrunt_dir()}/../../terragrunt.hcl"
  expose = true
}

terraform {
  source = "../..//terraform-resources"
}

locals {
  name_prefix = "miri"
}

inputs = merge(
  include.base.locals,
  {
    name_prefix         = local.name_prefix
    resource_group_name = "${include.base.locals.resource_group_prefix}-${local.name_prefix}"

    vnet_address_space_prefix = "10.0"
    vm_public_ssh_keys        = ["~/.ssh/2024-06_tf-workshop.pub"]
  }
)

remote_state {
  backend = include.base.remote_state.backend
  config = merge(
    include.base.remote_state.config,
    {
      key = "miri-terragrunt.tfstate"
    }
  )
}

